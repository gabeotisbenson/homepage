const typeValidator = type => [
	'string',
	'number',
	'boolean',
	'object',
	'function'
].indexOf(type) !== -1;

const keyTypeKeys = [
	{ key: 'key', type: 'string', required: true },
	{
		key: 'type',
		type: 'string',
		required: true,
		validator: typeValidator
	},
	{ key: 'required', type: 'boolean' },
	{ key: 'validator', type: 'function' }
];

export const isValidKeyType = keyType => keyTypeKeys
	.reduce((isValid, keyTypeKey) => {
		if (!isValid) return false;
		const { required } = keyTypeKey;
		const missing = typeof keyType[keyTypeKey.key] === 'undefined';
		if (required && missing) return false;
		if (typeof keyType[keyTypeKey.key] !== keyTypeKey.type) return false;

		return isValid;
	}, true);

export const isValidKeyTypeList = keyTypes => keyTypes
	.reduce((isValid, keyType) => {
		if (!isValid) return false;
		if (!isValidKeyType(keyType)) return false;

		return isValid;
	}, true);

export const getTemplateFromKeyTypes = keyTypes => keyTypes
	.reduce((template, keyType) => {
		if (keyType.required) template[keyType.key] = keyType.default || null;

		return template;
	}, {});

export const isValidObjectWithKeyTypes = (object, keyTypes) => {
	if (!isValidKeyTypeList(keyTypes)) return false;

	return keyTypes
		.reduce((isValid, { required, key, type, validator }) => {
			if (!isValid) return false;
			const { [key]: value } = object;
			if (required && typeof value === 'undefined') return false;
			if (typeof value !== type) return false;
			if (validator && !validator(value)) return false;

			return isValid;
		}, true);
};
