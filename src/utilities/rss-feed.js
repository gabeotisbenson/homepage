import {
	getTemplateFromKeyTypes,
	isValidObjectWithKeyTypes
} from '~/utilities/key-type';

export const rssFeedKeyTypes = [
	{ key: 'title', type: 'string', required: true },
	{ key: 'url', type: 'string', required: true },
	{ key: 'homeUrl', type: 'string' },
	{ key: 'titleField', type: 'string', required: true },
	{ key: 'urlField', type: 'string', required: true },
	{ key: 'dateField', type: 'string', required: true },
	{ key: 'authorField', type: 'string' }
];

export const rssFeedTemplate = getTemplateFromKeyTypes(rssFeedKeyTypes);
export const isValidRssFeed = isValidObjectWithKeyTypes(rssFeedKeyTypes);
