import { gteZero } from '~/utilities/number';
import {
	getTemplateFromKeyTypes,
	isValidObjectWithKeyTypes
} from '~/utilities/key-type';

export const coinKeyTypes = [
	{ key: 'symbol', type: 'string', required: true },
	{ key: 'id', type: 'string', required: true },
	{ key: 'name', type: 'string' },
	{
		key: 'balance',
		type: 'number',
		required: true,
		validator: gteZero
	},
	{ key: 'value', type: 'number', validator: gteZero }
];

export const coinTemplate = getTemplateFromKeyTypes(coinKeyTypes);

// eslint-disable-next-line max-len, vue/max-len
export const isValidCoin = coin => isValidObjectWithKeyTypes(coin, coinKeyTypes);
