export const titleCase = str => String(str)
	.replace(/-+/gu, ' ')
	.replace(/ +/gu, ' ')
	.split(' ')
	.map(s => s.charAt(0).toUpperCase() + s.substring(1))
	.join(' ');

export const slugCase = str => String(str)
	.replace(/[^a-zA-Z0-9-]/gu, '-')
	.replace(/-+/gu, '-')
	.toLowerCase();
