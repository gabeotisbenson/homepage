// eslint-disable-next-line no-process-env
export default process.env.NODE_ENV === 'development' ? 'https://cors-anywhere.herokuapp.com/' : 'https://cors.gabe.xyz/';
