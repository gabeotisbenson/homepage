/* eslint no-magic-numbers: off */

const MS_IN_A_SECOND = 1000;

export const ONE_SECOND = MS_IN_A_SECOND;

export const HALF_SECOND = MS_IN_A_SECOND / 2;
