export const getLatLong = () => new Promise((resolve, reject) => {
	const onSuccess = location => resolve({
		latitude: location.coords.latitude,
		longitude: location.coords.longitude
	});
	const onFailure = err => reject(err);
	const options = {
		timeout: 3000,
		enableHighAccuracy: true
	};
	navigator.geolocation.getCurrentPosition(onSuccess, onFailure, options);
});
