import fetchJsonp from 'fetch-jsonp';
import { getLatLong } from '~/modules/location';
import log from '~/modules/log';

const requestParams = [
	{
		key: 'exclude',
		value: [
			'alerts',
			'currently',
			'flags',
			'hourly',
			'minutely'
		].join(',')
	},
	{ key: 'lang', value: 'en' },
	{ key: 'units', value: 'us' }
];

const requestParamStr = requestParams
	.map(param => `${param.key}=${param.value}`)
	.join('&');

const getForecastUrl = (apiKey, { latitude, longitude }) => `https://api.darksky.net/forecast/${apiKey}/${latitude},${longitude}?${requestParamStr}`;

export const getForecast = async apiKey => {
	try {
		const latLong = await getLatLong();
		const url = getForecastUrl(apiKey, latLong);
		const response = await fetchJsonp(url);
		const { daily: { data } } = await response.json();

		return data;
	} catch (err) {
		log.error(err, 'Failed to fetch weather forecast.');

		return [];
	}
};
