module.exports = {
	plugins: {
		'node-css-mqpacker': true,
		'postcss-preset-env': true,
		autoprefixer: true,
		cssnano: {
			preset: [
				'default',
				{
					discardComments: {
						removeAll: true
					}
				}
			]
		}
	}
};
